const Actifsnoncourants = require('../models/actifsnoncourants.model.js');
// Retourner tout les actifs courants
exports.findAll = (req, res) => {
Actifsnoncourants.find()
  .then(actifsnoncourants => {
  res.send(actifsnoncourants);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Erreur lors de la récupération des actifs non courants."
});
});
};
// Creer et sauvegarder un nouvel actif courant
exports.create = (req, res) => {
// Validate request
if(!req.body) {
  return res.status(400).send({
  message: "Veuillez remplir tout les champs"
});
}
// Creer actifs courants
const actifsnoncourants = new Actifsnoncourants({
    immo_incorp : req.body.immo_incorp,
    ammor_immo_incorp : req.body.ammor_immo_incorp,
    immo_corp : req.body.immo_corp,
    ammor_immo_corp : req.body.ammor_immo_corp,
    immo_fin : req.body.immo_fin,
    prov_immo_fin : req.body. prov_immo_fin,
    autres_actifs_noncour : req.body.autres_actifs_noncour
});
// Sauvegarder les actifs courants dans la base
actifsnoncourants.save()
  .then(data => {
  res.send(data);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Erreur lors de la création des actifs non courants."
});
});
};
// Find a single User with a id
exports.findOne = (req, res) => {
 Actifsnoncourants.findById(req.params.id)
  .then(actifsnoncourants => {
  if(!actifsnoncourants) {
   return res.status(404).send({
   message: "actif non courant non trouvé avecl'id " + req.params.id
 });
}
 res.send(actifsnoncourants);
}).catch(err => {
  if(err.kind === 'ObjectId') {
    return res.status(404).send({
    message: "actif non courant non trouvé avecl'id " + req.params.id
  });
}
return res.status(500).send({
  message: "Error getting actif non courant with id " + req.params.id
});
});
};
// Modifier actif courant identifié par son id 
exports.update = (req, res) => {
// Validate Request
if(!req.body) {
  return res.status(400).send({
  message: "Remplir tout les champs"
});
}
// trouver actif courant et le modifier avec le request body 
Actifsnoncourants.findByIdAndUpdate(req.params.id, {
    immo_incorp : req.body.immo_incorp,
    ammor_immo_incorp : req.body.ammor_immo_incorp,
    immo_corp : req.body.immo_corp,
    ammor_immo_corp : req.body.ammor_immo_corp,
    immo_fin : req.body.immo_fin,
    prov_immo_fin : req.body. prov_immo_fin,
    autres_actifs_noncour : req.body.autres_actifs_noncour
}, {new: true})
.then(actifsnoncourants => {
 if(!actifsnoncourants) {
   return res.status(404).send({
   message: "actif non courant non trouvé avec l'id " + req.params.id
 });
}
res.send(actifsnoncourants);
}).catch(err => {
if(err.kind === 'ObjectId') {
  return res.status(404).send({
  message: "actif non courant non trouvé avec l'id  " + req.params.id
});
}
return res.status(500).send({
  message: "Erreur modification de l'actif non courant avec l'id  " + req.params.id
});
});
};
