const Actifscourants = require('../models/actifscourants.model.js');
// Retourner tout les actifs courants
exports.findAll = (req, res) => {
Actifscourants.find()
  .then(actifscourants => {
  res.send(actifscourants);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Erreur lors de la récupération des actifs courants."
});
});
};
// Creer et sauvegarder un nouvel actif courant
exports.create = (req, res) => {
// Validate request
if(!req.body) {
  return res.status(400).send({
  message: "Veuillez remplir tout les champs"
});
}
// Creer actifs courants
const actifscourants = new Actifscourants({
  stock : req.body.stock,
  client_comp_ratt : req.body.client_comp_ratt,
  Plac_autres_act_fin : req.body.Plac_autres_act_fin,
  liq_eq_liq : req.body.liq_eq_liq,
  prov_stock : req.body.prov_stock,
  prov_client : req.body.prov_client
});
// Sauvegarder les actifs courants dans la base
actifscourants.save()
  .then(data => {
  res.send(data);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Erreur lors de la création des actifs courants."
});
});
};
// Find a single User with a id
exports.findOne = (req, res) => {
 Actifscourants.findById(req.params.id)
  .then(actifscourants => {
  if(!actifscourants) {
   return res.status(404).send({
   message: "actif courant non trouvé avecl'id " + req.params.id
 });
}
 res.send(actifscourants);
}).catch(err => {
  if(err.kind === 'ObjectId') {
    return res.status(404).send({
    message: "actif courant non trouvé avecl'id " + req.params.id
  });
}
return res.status(500).send({
  message: "Error getting actif courant with id " + req.params.id
});
});
};
// Modifier actif courant identifié par son id 
exports.update = (req, res) => {
// Validate Request
if(!req.body) {
  return res.status(400).send({
  message: "Remplir tout les champs"
});
}
// trouver actif courant et le modifier avec le request body 
Actifscourants.findByIdAndUpdate(req.params.id, {
    stock : req.body.stock,
    client_comp_ratt : req.body.client_comp_ratt,
    Plac_autres_act_fin : req.body.Plac_autres_act_fin,
    liq_eq_liq : req.body.liq_eq_liq,
    prov_stock : req.body.prov_stock,
    prov_client : req.body.prov_client
}, {new: true})
.then(actifscourants => {
 if(!actifscourants) {
   return res.status(404).send({
   message: "actif courant non trouvé avec l'id " + req.params.id
 });
}
res.send(actifscourants);
}).catch(err => {
if(err.kind === 'ObjectId') {
  return res.status(404).send({
  message: "actif courant non trouvé avec l'id  " + req.params.id
});
}
return res.status(500).send({
  message: "EErreur modification de l'actif courant avec l'id  " + req.params.id
});
});
};
