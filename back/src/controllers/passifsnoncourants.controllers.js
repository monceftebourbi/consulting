const Passifsnoncourants = require('../models/passifsnoncourants.model.js');
// Retourner tout les passifs courants
exports.findAll = (req, res) => {
Passifsnoncourants.find()
  .then(passifsnoncourants => {
  res.send(passifsnoncourants);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Erreur lors de la récupération des passifs non courants."
});
});
};
// Creer et sauvegarder un nouvel passif non courant
exports.create = (req, res) => {
// Validate request
if(!req.body) {
  return res.status(400).send({
  message: "Veuillez remplir tout les champs"
});
}
// Creer passifs non courants
const passifsnoncourants = new Passifsnoncourants({
    emprunts : req.body.emprunts ,
    autres_pass_fin : req.body.autres_pass_fin,
    provisions : req.body.provisions
  
});
// Sauvegarder les passifs non courants dans la base
passifsnoncourants.save()
  .then(data => {
  res.send(data);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Erreur lors de la création des passifs non courants."
});
});
};
// trouver un seul passif non courant avec son id
exports.findOne = (req, res) => {
 Passifsnoncourants.findById(req.params.id)
  .then(passifsnoncourants => {
  if(!passifsnoncourants) {
   return res.status(404).send({
   message: "passif non courant non trouvé avecl'id " + req.params.id
 });
}
 res.send(passifsnoncourants);
}).catch(err => {
  if(err.kind === 'ObjectId') {
    return res.status(404).send({
    message: "passif non courant non trouvé avecl'id " + req.params.id
  });
}
return res.status(500).send({
  message: "Error getting passif non courant with id " + req.params.id
});
});
};
// Modifier passif non courant identifié par son id 
exports.update = (req, res) => {
// Validate Request
if(!req.body) {
  return res.status(400).send({
  message: "Remplir tout les champs"
});
}
// trouver passif courant et le modifier avec le request body 
Passifsnoncourants.findByIdAndUpdate(req.params.id, {
    emprunts : req.body.emprunts ,
    autres_pass_fin : req.body.autres_pass_fin,
    provisions : req.body.provisions

}, {new: true})
.then(passifsnoncourants => {
 if(!passifsnoncourants) {
   return res.status(404).send({
   message: "passif non courant non trouvé avec l'id " + req.params.id
 });
}
res.send(passifsnoncourants);
}).catch(err => {
if(err.kind === 'ObjectId') {
  return res.status(404).send({
  message: "passif non courant non trouvé avec l'id  " + req.params.id
});
}
return res.status(500).send({
  message: "Erreur modification du passif non courant avec l'id  " + req.params.id
});
});
};