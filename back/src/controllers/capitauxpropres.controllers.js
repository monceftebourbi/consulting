const Capitauxpropres = require('../models/capitauxpropres.model.js');
// Retourner tout les capitaux propres
exports.findAll = (req, res) => {
capitauxpropres.find()
  .then(capitauxpropres => {
  res.send(capitauxpropres);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Erreur lors de la récupération des actifs courants."
});
});
};
// Creer et sauvegarder un nouvel actif courant
exports.create = (req, res) => {
// Validate request
if(!req.body) {
  return res.status(400).send({
  message: "Veuillez remplir tout les champs"
});
}
// Creer actifs courants
const capitauxpropres = new Capitauxpropres({
    capital  : req.body.capital ,
    reserves  : req.body.reserves ,
    autre_cap_prop : req.body.autre_cap_prop,
    res_repor : req.body.res_repor
  
});
// Sauvegarder les capitaux propres dans la base
capitauxpropres.save()
  .then(data => {
  res.send(data);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Erreur lors de la création des capitaux propres."
});
});
};
// trouver un seul capital propre avec l'id
exports.findOne = (req, res) => {
    Capitauxpropres.findById(req.params.id)
  .then(capitauxpropres => {
  if(!capitauxpropres) {
   return res.status(404).send({
   message: "capitaux propres non trouvé avecl'id " + req.params.id
 });
}
 res.send(capitauxpropres);
}).catch(err => {
  if(err.kind === 'ObjectId') {
    return res.status(404).send({
    message: "Capitaux propres non trouvés avecl'id " + req.params.id
  });
}
return res.status(500).send({
  message: "Error getting capitaux propres with id " + req.params.id
});
});
};
// Modifier capitaux propres identifiés par son id 
exports.update = (req, res) => {
// Validate Request
if(!req.body) {
  return res.status(400).send({
  message: "Remplir tout les champs"
});
}
// trouver capitaux propres et le modifier avec le request body 
Capitauxpropres.findByIdAndUpdate(req.params.id, {
    capital  : req.body.capital ,
    reserves  : req.body.reserves ,
    autre_cap_prop : req.body.autre_cap_prop,
    res_repor : req.body.res_repor
}, {new: true})
.then(capitauxpropres => {
 if(!capitauxpropres) {
   return res.status(404).send({
   message: "capitaux propres non trouvés avec l'id " + req.params.id
 });
}
res.send(capitauxpropres);
}).catch(err => {
if(err.kind === 'ObjectId') {
  return res.status(404).send({
  message: "capitaux propres non trouvés avec l'id  " + req.params.id
});
}
return res.status(500).send({
  message: "Erreur modification des capitaux propres avec l'id  " + req.params.id
});
});
};
