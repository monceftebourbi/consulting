const Passifscourants = require('../models/passifscourants.model.js');
// Retourner tout les passifs courants
exports.findAll = (req, res) => {
Passifscourants.find()
  .then(passifscourants => {
  res.send(passifscourants);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Erreur lors de la récupération des passifs courants."
});
});
};
// Creer et sauvegarder un nouvel passif courant
exports.create = (req, res) => {
// Validate request
if(!req.body) {
  return res.status(400).send({
  message: "Veuillez remplir tout les champs"
});
}
// Creer passifs courants
const passifscourants = new Passifscourants({
    four_compte_ratt : req.body.four_compte_ratt ,
    autres_passifscour : req.body.autres_passifscour,
    conc_banq_pass_fin : req.body.conc_banq_pass_fin
  
});
// Sauvegarder les passifs courants dans la base
passifscourants.save()
  .then(data => {
  res.send(data);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Erreur lors de la création des passifs courants."
});
});
};
// trouver un seul passif courant avec son id
exports.findOne = (req, res) => {
 Passifscourants.findById(req.params.id)
  .then(passifscourants => {
  if(!passifscourants) {
   return res.status(404).send({
   message: "passif courant non trouvé avecl'id " + req.params.id
 });
}
 res.send(passifscourants);
}).catch(err => {
  if(err.kind === 'ObjectId') {
    return res.status(404).send({
    message: "passif courant non trouvé avecl'id " + req.params.id
  });
}
return res.status(500).send({
  message: "Error getting passif courant with id " + req.params.id
});
});
};
// Modifier passif courant identifié par son id 
exports.update = (req, res) => {
// Validate Request
if(!req.body) {
  return res.status(400).send({
  message: "Remplir tout les champs"
});
}
// trouver passif courant et le modifier avec le request body 
Passifscourants.findByIdAndUpdate(req.params.id, {
    four_compte_ratt : req.body.four_compte_ratt ,
    autres_passifscour : req.body.autres_passifscour,
    conc_banq_pass_fin : req.body.conc_banq_pass_fin

}, {new: true})
.then(passifscourants => {
 if(!passifscourants) {
   return res.status(404).send({
   message: "passif courant non trouvé avec l'id " + req.params.id
 });
}
res.send(passifscourants);
}).catch(err => {
if(err.kind === 'ObjectId') {
  return res.status(404).send({
  message: "passif courant non trouvé avec l'id  " + req.params.id
});
}
return res.status(500).send({
  message: "Erreur modification du passif courant avec l'id  " + req.params.id
});
});
};
