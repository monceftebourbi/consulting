const Etat_res = require('../models/etatderesultat.model');
// Retrieve and return all etats de resultat from the database.
exports.findAll = (req, res) => {
Etat_res.find()
  .then(etat_res => {
  res.send(etat_res);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Something went wrong while getting list of etat de resultat."
});
});
};
// Create and Save a new etat de resultat
exports.create = (req, res) => {
// Validate request
if(!req.body) {
  return res.status(400).send({
  message: "Please fill all required field"
});
}
// Create a new etat de resultat
const etat_res = new Etat_res({

    revenus : req.body.revenus,
    prod_exploit: req.body.prod_exploit,
    prod_immo : req.body.prod_immo,
    total_pr_exploit: req.body.total_pr_exploit,
    charges_exploit: req.body.charges_exploit,
    var_stocks : req.body.var_stocks,
    ach_mar_cons: req.body.ach_mar_cons,
    ach_app_cons : req.body.ach_app_cons,
    charges_perso: req.body.charges_perso,
    dot_ammo_prov: req.body.dot_ammo_prov,
    autres_charges_exploit : req.body.autres_charges_exploit,
    total_charges_exploit : req.body.total_charges_exploit,
    res_exploit : req.body.res_exploit,
    charges_fin : req.body.charges_fin,
    prod_placements: req.body.prod_placements,
    autres_gains_ord  : req.body.autres_gains_ord ,
    autres_pertes_ord : req.body.autres_pertes_ord,
    res_act_ord_avant : req.body.res_act_ord_avant,
    impot_benef : req.body.impot_benef,
    res_act_ord_apres : req.body.res_act_ord_apres,
    ele_extra  : req.body.ele_extra,
    res_net : req.body.res_net,
    effet_modif_comptables : req.body.effet_modif_comptables,
    res_apres_modif : req.body.res_apres_modif
    
});
// Save etatderesultat in the database
etat_res.save()
  .then(data => {
  res.send(data);
}).catch(err => {
  res.status(500).send({
  message: err.message || "Something went wrong while creating new etat de resultat."
});
});
};
// Find a single etat de resultat with a id
exports.findOne = (req, res) => {
 Etat_res.findById(req.params.id)
  .then(etat_res => {
  if(!etat_res) {
   return res.status(404).send({
   message: "etat de resultat not found with id " + req.params.id
 });
}
 res.send(etat_res);
}).catch(err => {
  if(err.kind === 'ObjectId') {
    return res.status(404).send({
    message: "etat de resultat not found with id " + req.params.id
  });
}
return res.status(500).send({
  message: "Error getting etat de resultat with id " + req.params.id
});
});
};
// Update a etat de resultat identified by the id in the request
exports.update = (req, res) => {
// Validate Request
if(!req.body) {
  return res.status(400).send({
  message: "Please fill all required field"
});
}
// Find etat de resultat and update it with the request body
Etat_res.findByIdAndUpdate(req.params.id, {

    revenus : req.body.revenus,
    prod_exploit: req.body.prod_exploit,
    prod_immo : req.body.prod_immo,
    total_pr_exploit: req.body.total_pr_exploit,
    charges_exploit: req.body.charges_exploit,
    var_stocks : req.body.var_stocks,
    ach_mar_cons: req.body.ach_mar_cons,
    ach_app_cons : req.body.ach_app_cons,
    charges_perso: req.body.charges_perso,
    dot_ammo_prov: req.body.dot_ammo_prov,
    autres_charges_exploit : req.body.autres_charges_exploit,
    total_charges_exploit : req.body.total_charges_exploit,
    res_exploit : req.body.res_exploit,
    charges_fin : req.body.charges_fin,
    prod_placements: req.body.prod_placements,
    autres_gains_ord  : req.body.autres_gains_ord ,
    autres_pertes_ord : req.body.autres_pertes_ord,
    res_act_ord_avant : req.body.res_act_ord_avant,
    impot_benef : req.body.impot_benef,
    res_act_ord_apres : req.body.res_act_ord_apres,
    ele_extra  : req.body.ele_extra,
    res_net : req.body.res_net,
    effet_modif_comptables : req.body.effet_modif_comptables,
    res_apres_modif : req.body.res_apres_modif

}, {new: true})
.then(etat_res => {
 if(!etat_res) {
   return res.status(404).send({
   message: "etat de resultat not found with id " + req.params.id
 });
}
res.send(etat_res);
}).catch(err => {
if(err.kind === 'ObjectId') {
  return res.status(404).send({
  message: "etat de resultat not found with id " + req.params.id
});
}
return res.status(500).send({
  message: "Error updating etat de resultat with id " + req.params.id
});
});
};
// Delete a etat de resultat with the specified id in the request
exports.delete = (req, res) => {
Etat_res.findByIdAndRemove(req.params.id)
.then(etat_res => {
if(!etat_res) {
  return res.status(404).send({
  message: "etat de resultat not found with id " + req.params.id
});
}
res.send({message: "etat de resultat deleted successfully!"});
}).catch(err => {
if(err.kind === 'ObjectId' || err.name === 'NotFound') {
  return res.status(404).send({
  message: "etat de resultat not found with id " + req.params.id
});
}
return res.status(500).send({
  message: "Could not delete etat de resultat with id " + req.params.id
});
});
};