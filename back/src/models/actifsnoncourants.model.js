const mongoose = require('mongoose');

const ActifsnoncourantsSchema = mongoose.Schema({
    // immobilisations incorporelles
    immo_incorp: { type : Number , required : true },
    // ammortissement immobilisations incorporelles
    ammor_immo_incorp: { type : Number , required : true },
    // immobilisations corporelles
    immo_corp: { type : Number , required : true },
    // ammortissement immobilisations corporelles
    ammor_immo_corp : {type : Number , required : true},
    // immobilisations financieres
    immo_fin : { type : Number , required : true},
    // provision immobilisations financieres
    prov_immo_fin : {type : Number , required : true},
    //autres actifs non courant
    autres_actifs_noncour : {type : Number , required : true}
    
}, {
    timestamps: true
});

module.exports = ActifsnoncourantsSchema;