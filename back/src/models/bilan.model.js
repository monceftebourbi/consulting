var mongoose = require('mongoose');
const beautifyUnique = require('mongoose-beautiful-unique-validation');
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);


const Actifscourants= require('./actifscourants.model.js');
const actifsnoncourants = require('./actifsnoncourants.model');
const capitauxpropres = require('./capitauxpropres.model');
const passifscourants = require('./passifscourants.model');
const passifsnoncourants = require('./passifsnoncourants.model');



var BilanSchema = new mongoose.Schema({
    actifscourants : Actifscourants,
    actifsnoncourants : actifsnoncourants,
    capitauxpropres : capitauxpropres,
    passifscourants : passifscourants,
    passifsnoncourants : passifsnoncourants

     }, {timestamps: true});

  BilanSchema.plugin(beautifyUnique, {
    defaultMessage: "Une erreur c\'est produite: "
});

const Bilan = module.exports = mongoose.model('bilan', BilanSchema);