const mongoose = require('mongoose');

const PassifsnoncourantsSchema = mongoose.Schema({

    emprunts : { type : Number , required : true },
    // autres passifs financiers
    autres_pass_fin : { type : Number , required : true },
    provisions : { type : Number , required : true }
   
}, {
    timestamps: true
});

module.exports = PassifsnoncourantsSchema;