const mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
const beautifyUnique = require('mongoose-beautiful-unique-validation');

const UserSchema = mongoose.Schema({
    raisonsocial: { type : String , required : true, unique : 'la raison sociale est deja utilisée'},
    formejuridique: { type : String , required : true , unique : false},
    secteur: {type : String, required : true , unique : false},
    autresecteur: {type : String, required : function(){return this.secteur =='Autres services'}, unique : false},
    tel: {type :Number, required : true , unique : false},
    Date: { type : Date , required : true , unique : false},
    sigle: {type : String, required : true , unique : false},
    codepostal: {type :Number, required : true , unique : false},
    ville: { type : String , required : true , unique : false},
    pays: {type : String, required : true , unique : false},
    etat: {type : String, required : true ,unique : false},
    fax: {type :Number, required : false , unique : false},
    password : {type:String, required:true, unique:false},
    enseigne: { type:String,required:true,unique: 'Enseigne  \"{VALUE}\" déja utilisée'},
    matriculefiscal : {type:String,required:true,unique: 'Le matricule fiscal \"{VALUE}\" est déja utilisé'},
    adresse : {type:String,required:true,unique:false},
    representant : {type:String,required:true,unique:false},
    email : { type:String,required:true,unique:false},

    is_active:  { type: Boolean, default: false },
    is_verified:  { type: Boolean, default: false },
    is_deleted:  { type: Boolean, default: false }
}, {
    timestamps: true
});

UserSchema.plugin(beautifyUnique, {
  defaultMessage: "Une erreur c\'est produite: "
});

UserSchema.pre('save', function(next) {
    var user = this;
    bcrypt.hash(user.password, null , null , function(err,hash){
      if(err) return next(err);
        user.password = hash;
        next();
    });
  });

module.exports = mongoose.model('User', UserSchema);

