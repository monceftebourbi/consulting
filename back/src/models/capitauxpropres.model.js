const mongoose = require('mongoose');

const CapitauxpropresSchema = mongoose.Schema({
    capital : { type : Number , required : true },
    reserves : { type : Number , required : true },
    // autres capitaux propres
    autre_cap_prop : { type : Number , required : true },
    // ??
    res_repor : {type : Number , required : true}
    
}, {
    timestamps: true
});

module.exports = CapitauxpropresSchema ;