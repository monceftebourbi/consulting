const mongoose = require('mongoose');

const EtatderesultatSchema = mongoose.Schema({  
//Revenus 
revenus : { type : Number , required : true },
//Produits d'exploitation 
prod_exploit : { type : Number , required : true },
//Production immobilisée 
prod_immo : { type : Number , required : true },
//Total des produits d'exploitation 
total_pr_exploit : { type : Number , required : true },
//Charges d'exploitation 
charges_exploit : { type : Number , required : true },
//Variation des stocks des produits finis et des encours (en+ou-) 
var_stocks : { type : Number , required : true },
//Achats de marchandises consommés 
ach_mar_cons : { type : Number , required : true },
//Achats d'approvisionnements consommés 
ach_app_cons : { type : Number , required : true },
//Charges de personnel 
charges_perso : { type : Number , required : true },
//Dotations aux amortissements et aux provisions 
dot_ammo_prov : { type : Number , required : true },
//Autres charges d'exploitation 
autres_charges_exploit : { type : Number , required : true },
//Total des charges d'exploitation (x) (x)
total_charges_exploit : { type : Number , required : true },
//Résultat d'exploitation 
res_exploit : { type : Number , required : true },
//Charges financières nettes (x) (x)
charges_fin : { type : Number , required : true },
//Produits des placements 
prod_placements : { type : Number , required : true },
//Autres gains ordinaires 
autres_gains_ord : { type : Number , required : true },
//Autres pertes ordinaires (x) (x)
 autres_pertes_ord: { type : Number , required : true },
//Résultat des activités ordinaires avant impôt
res_act_ord_avant : { type : Number , required : true },
//Impôt sur les bénéfices (x) (x)
impot_benef : { type : Number , required : true },
//Résultat des activités ordinaires après impôt 
res_act_ord_apres : { type : Number , required : true },
//Eléments extraordinaires (Gains/Pertes) 
ele_extra : { type : Number , required : true },
//Résultat net de l’exercice 
res_net : { type : Number , required : true },
//Effet des modifications comptables (net d'impôt) 
effet_modif_comptables : { type : Number , required : true },
//Résultat après modifications comptables
res_apres_modif : { type : Number , required : true }
}, {
    timestamps: true
});

const Etatderesultat = module.exports = mongoose.model('etat', EtatderesultatSchema);