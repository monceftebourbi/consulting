const mongoose = require('mongoose');

const ActifscourantsSchema = mongoose.Schema({
    stock : { type : Number , required : true },
    // clients et comptes rattachés 
    client_comp_ratt: { type : Number , required : true },
    // placements et autres actifs courants
    Plac_autres_act_fin : { type : Number , required : true },
    // liquidité et équivalent de liquidité
    liq_eq_liq : {type : Number , required : true},
    // provisions stock
    prov_stock : { type : Number , required : true},
    // provision client
    prov_client : {type : Number , required : true},
    
}, {
    timestamps: true
});

module.exports = ActifscourantsSchema ;