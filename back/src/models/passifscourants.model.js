const mongoose = require('mongoose');

const PassifscourantsSchema = mongoose.Schema({
    // fournisseurs et comptes rattachés 
    four_compte_ratt : { type : Number , required : true },
    // autres passifs courants
    autres_passifscour : { type : Number , required : true },
    // concours banquaires
    conc_banq_pass_fin : { type : Number , required : true },
   
}, {
    timestamps: true
});

module.exports = PassifscourantsSchema;