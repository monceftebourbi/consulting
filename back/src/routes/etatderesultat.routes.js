const express = require('express')
const router = express.Router()
const etatderesultatController = require('../controllers/etatderesultat.controllers');
// Retrieve all etat de resultat
router.get('/', etatderesultatController.findAll);
// Create a etat de resultat
router.post('/create', etatderesultatController.create);
// Retrieve a single etat de resultat with id
router.get('/:id', etatderesultatController.findOne);
// Update a etat de resultat with id
router.put('/:id', etatderesultatController.update);
// Delete a etat de resultat with id
router.delete('/:id', etatderesultatController.delete);

module.exports = router