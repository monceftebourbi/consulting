const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');


// create express app
const app = express();
// Setup server port
const port = process.env.PORT || 4000;
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse requests of content-type - application/json
app.use(bodyParser.json())

//
app.use(cors());
// Configuring the database
const dbConfig = require('./config/db_config.js');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
// Connecting to the database
mongoose.connect(dbConfig.url, {
useNewUrlParser: true,
useUnifiedTopology: true
}).then(() => {
  console.log("Successfully connected to the database");
}).catch(err => {
  console.log('Could not connect to the database.', err);
  process.exit();
});
// define a root/default route
app.get('/', (req, res) => {
   res.json({"message": " creation"});
});

// Require Users routes
const userRoutes = require('./src/routes/user.routes');
// using as middleware
app.use('/api/users', userRoutes);


// Require bilan routes
const bilanRoutes = require('./src/routes/bilan.routes');
// using as middleware
app.use('/api/bilan', bilanRoutes);

// Require bilan routes
const etatRoutes = require('./src/routes/etatderesultat.routes');
// using as middleware
app.use('/api/etatderesultat', etatRoutes);



// listen for requests
app.listen(port, () => {
   console.log(`Node server is listening on port ${port}`);
});