import { Component, OnInit, Inject, Input, ElementRef, ViewChild } from '@angular/core';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import {DemandeService} from '../../../../services/demande/demande.service';
import {Router, ActivatedRoute} from '@angular/router' ;
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {EmployeeService} from '../../../../services/employee/employee.service' ;
import {AuthService} from '../../../../services/auth/auth.service' ;
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {NotificationService} from '../../../../services/notification/notification.service';

export interface linkModel{
  url? : string ;
  params? : string ;
  paramsValue?:string ;
}

@Component({
  selector: 'app-conge',
  templateUrl: './conge.component.html',
  styleUrls: ['./conge.component.scss'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'fr'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ]
})
export class CongeComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('closeModal', {static: true} ) closeModal: ElementRef
  form: FormGroup ;
  conge: any;
  type: string;
  conges : any;
  holidays = [];
  rhUsers : any;
  user = this.authService.getUserfromToken();
  maxConge = 0;
  congeUtilise = 0;
  realPeriode = 0;
  public notification: any ;
  public notifLink: linkModel = {} ;
  types = [
    {nom:'Congé annuel',dure:'0'},
    {nom:'Congé sans solde',dure:'-1'},
    {nom:'Naissance d\'un enfant (2 Jrs)',dure:'2'},
    {nom:'Décès d\'un conjoint (3 Jrs)',dure:'3'},
    {nom:'Décès d\'un père, d\'une mère ou d\'un fils (3 Jrs)',dure:'3'},
    {nom:'Décès d\'un frère, d\'une sœur, d\'un petit-fils, d\'une petite fille, d\'un grand père ou d\'une grande mère (2 Jrs)',dure:'2'},
    {nom:'Mariage d\'un enfant (1 Jrs)',dure:'1'},
    {nom:'Circoncision d\'un enfant (1 Jrs)',dure:'1'},
  ]
  @Input() id : string;

  displayedColumns: string[] = ['Type','Datesoumission','Datedebut','Datefin','Etat'];
  MyDataSource: any;

  constructor(
    private fb: FormBuilder,
    private demandeService : DemandeService,
    private router: Router,
    private snackBar: MatSnackBar,
    private authService: AuthService,
    private employeeService : EmployeeService,
    private notificationService : NotificationService    
  ) { 
    if(this.user.situationfamiliale != 'célibataire'){
      this.types.push({nom:'Mariage d\'un enfant (3 Jrs)',dure:'3'})

    }
    this.getSoldeCogeConsomme();  
    this.fetchUsers();
    this.fetchHolidays();
    this.form = this.fb.group({
      type: ['', Validators.required],
      datedebut: ['', Validators.required],
      datefin: ['', Validators.required],
      periode: new FormControl('', [
        Validators.required,
        Validators.min(0),
      ]),
    });
  }

  ngOnInit() {
    this.fetchConge();
  }

  onSubmit(){
    if(this.form.valid){
      let conge = this.form.value;

      conge.destinataire = [...new Set([...this.rhUsers.map(user => user._id) ,...this.user.responsable])];
      conge.emmeteur = this.user._id
      conge.user = this.user._id
      conge.solde = this.maxConge;
      this.demandeService.createConge(conge).subscribe(data => {
       if (data.success) {
        this.notifLink.url = '/conges-Rh';
        this.notification = {
          'sender': conge.emmeteur,
          'receiver': conge.destinataire,
          'title': 'Nouvelle demande de congé',
          'image': this.user.image,
          'message': 'Demande pour  '+this.user.fname+' '+ this.user.lname,
          'link': this.notifLink,
        };
        this.notificationService.createNotification(JSON.stringify(this.notification)).subscribe(data2 => {
          if (data2.success) {
            console.log(this.notification);
            console.log(data2);
          }
         });
         this.snackBar.open(data.msg, '✔', {
          verticalPosition: 'top',
          panelClass: ['btn-success'],
           duration: 6000
         });
         this.fetchConge();
       } else {
           this.snackBar.open(data.msg, 'X', {
            verticalPosition: 'top',
            panelClass: ['btn-danger'],
             duration: 6000
           });
       }
       this.closeModal.nativeElement.click() //<-- here
      });
    }
     
  }
  formvalueupdate(event,control) {
    this.form.get(control).setValue(event.target.value);
    this.form.get(control).updateValueAndValidity();
    // alert(this.form.get(control).value);
  }

  onChangeDate(){
    if(this.form.controls['datedebut'].valid && this.form.controls['datefin'].valid){
      this.form.get('periode').setValue(Math.abs(this.form.controls['datedebut'].value.diff(this.form.controls['datefin'].value, 'days')));
      let startDate = new Date(this.form.controls['datedebut'].value);
      let endDate = new Date(this.form.controls['datefin'].value);
    
    // Validate input
      if (endDate < startDate) {
        this.form.get('periode').setValue(0);
        return 0;
      }
    
    // Calculate days between dates
      let millisecondsPerDay = 86400 * 1000; // Day in milliseconds
      startDate.setHours(0, 0, 0, 1);  // Start just after midnight
      endDate.setHours(23, 59, 59, 999);  // End just before midnight
      let diff = endDate.getTime() - startDate.getTime();  // Milliseconds between datetime objects    
      let days = Math.ceil(diff / millisecondsPerDay);
      this.realPeriode = days;
      console.log(this.realPeriode);
      // Subtract two weekend days for every week in between
      let weeks = Math.floor(days / 7);
      days -= weeks * 2;
    
      // Handle special cases
      let startDay = startDate.getDay();
      let endDay = endDate.getDay();
        
      // Remove weekend not previously removed.   
      if (startDay - endDay > 1) {
        days -= 2;
      }
      // Remove start day if span starts on Sunday but ends before Saturday
      if (startDay == 0 && endDay != 6) {
        days--;  
      }
      // Remove end day if span ends on Saturday but starts after Sunday
      if (endDay == 6 && startDay != 0) {
        days--;
      }
      /* Here is the code */
      this.holidays.forEach(day => {
        let thisday = new Date(day);
        thisday.setHours(0, 0, 0, 1);
        if ((thisday.getTime() >= startDate.getTime()) && (thisday.getTime() <= endDate.getTime())) {
          /* If it is not saturday (6) or sunday (0), substract it */
          if ((new Date(day).getDay() % 6) != 0) {
            days--;
          }
        }
      });
      console.log(days);
      this.form.get('periode').setValue(days);
    }
    
 }


  fetchUsers(){
    this.demandeService.getGroupes()
    .subscribe(groupes => {
    this.employeeService.getUsers()
    .subscribe(users => {
     let havePermissions : Array<any> = [] ;
  
    users.forEach(user => {
      user.permissions.forEach(permission => {
        let group = groupes.filter(group => group._id == permission)[0];
        group.permissions.forEach(element => {
          if(element == 'rh'){
          havePermissions.push(user);
          return ;
          }
        });
      });
    });
    havePermissions = havePermissions.filter(function(elem, index, self) {
      return index === self.indexOf(elem);
  })
  this.rhUsers = havePermissions;
    }, err => {
      console.log(err);
    });
  }, err => {
    console.log(err);
  });
}
  
  fetchHolidays(){
    this.demandeService.getHolidays()
    .subscribe(holidays => {
      holidays.forEach(day => {
        this.holidays.push(new Date(day.date));
      });
  }, err => {
    console.log(err);
  });
}
  
getSoldeCogeConsomme(){
    this.demandeService.getSoldeCogeConsomme(this.user._id)
    .subscribe(congeUtilise => {
      if(congeUtilise.length!=0)
        this.congeUtilise = congeUtilise[0].periode;
      else
        this.congeUtilise = 0;
  }, err => {
    console.log(err);
  });
}
  

  onChangeCongetype(newValue) {
    console.log(newValue);
    this.type = newValue.nom;
      let date1 = new Date();
      let date2 = new Date(this.user.datecommencement);
      let diff = Math.floor(date1.getTime() - date2.getTime());
      let days = Math.floor(diff/(1000 * 3600 * 24));
      this.maxConge = Math.floor((days * (this.user.contrat.conge/12/30))-this.congeUtilise);
      

if(newValue.dure == '-1') {
    this.maxConge = 365;
  } else {
    if(this.maxConge <0 && newValue.dure != '0')
    this.maxConge = 0;
    this.maxConge = Number(this.maxConge) + Number(newValue.dure);
  }
  this.form.controls['periode'].setValidators([
    Validators.required,
    Validators.min(1),
    Validators.max(this.maxConge),
  ]);
  this.onChangeDate();
 }

 nowStringAsDate() {
  return new Date();
}

fetchConge(){
  console.log(this.id); 
  this.demandeService.getMyConges(this.id)
  .subscribe(res => {
    this.MyDataSource = new MatTableDataSource();
    this.MyDataSource.data = res;
    this.MyDataSource.paginator = this.paginator;
    this.MyDataSource.sort = this.sort;
  }, err => {
    console.log(err);
  });
}
 
}
