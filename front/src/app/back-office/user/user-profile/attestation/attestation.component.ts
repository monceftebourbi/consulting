import { Component, OnInit, ViewChild, Input, ChangeDetectorRef, ElementRef } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { environment } from '../../../../../environments/environment' ;
import {DemandeService} from '../../../../services/demande/demande.service';
import {ManagementService} from '../../../../services/management/management.service';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import {saveAs} from 'file-saver';

@Component({
  selector: 'app-attestation',
  templateUrl: './attestation.component.html',
  styleUrls: ['./attestation.component.scss']
})
export class AttestationComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('closeModal', {static: true} ) closeModal: ElementRef;
  submited = false;
  form: FormGroup ;
  uri = environment.baseUrl;
  mypermissions: any[];
  MyDataSource: any;
  displayedColumns: string[] = ['Type','Raison','Date','Etat'];
  docList = [] ;
  types = [
    'Attestation de travail',
    'Fiche de paie',
    'Ordre de mission'    
  ]
  @Input() id : string;
  confirmationId :string ;

  constructor(
              private demandeService: DemandeService,
              private fb: FormBuilder,
              private mService: ManagementService,
              private changeDetectorRef: ChangeDetectorRef,
              private snackBar: MatSnackBar,) { 
    
     
    this.form = this.fb.group({
      type: ['', Validators.required],
      raison: ['', Validators.required]
    });
              }

  ngOnInit() {
    this.fetchData();
  
  }
  
  fetchData(){
    this.demandeService.getAttestations(this.id)
        .subscribe(res => {
        this.MyDataSource = new MatTableDataSource();
        this.MyDataSource.data = res;
        this.MyDataSource.paginator = this.paginator;
        this.MyDataSource.sort = this.sort;
  }, err => {
    console.log(err);
  });

  }

  onSubmit(){
    if(this.form.valid){
      this.submited = true;
      let attestation = this.form.value;
      attestation.user = this.id;
      console.log(attestation);
      this.demandeService.addAttestation(attestation).subscribe(data => {
        if (data.success) {
          this.snackBar.open(data.msg, '✔', {
           verticalPosition: 'top',
           panelClass: ['btn-success'],
            duration: 6000
          });
          this.closeModal.nativeElement.click() //<-- here
          this.form.reset();
          this.fetchData();
        } else {
          this.snackBar.open(data.msg, 'X', {
            verticalPosition: 'top',
            panelClass: ['btn-danger'],
             duration: 6000
           });
        }
        this.submited = false;
       });
    }else{
      this.snackBar.open('Verifier les champs', 'X', {
          verticalPosition: 'top',
          panelClass: ['btn-danger'],
          duration: 6000
        });
      this.submited = false;
    }
}
download(file){
  var filename = file;
  let a = filename.split('.');
  a.pop();
  this.mService.downloadDoc(filename)
  .subscribe(
    data => saveAs(data,a.pop()),
    error => console.error(error)
  );
   }
    applyFilter(filterValue: string) {
      this.MyDataSource.filter = filterValue.trim().toLowerCase();
    }
  
}
