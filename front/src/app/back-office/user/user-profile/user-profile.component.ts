import { environment } from '../../../../environments/environment';
import { EmployeeService } from '../../../services/employee/employee.service';
import {Router, ActivatedRoute } from '@angular/router';
import {Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import {emailValidation, passValidation} from '../../../services/validations';
import { MatOption } from '@angular/material/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import {FileUploader } from 'ng2-file-upload';
import * as decode from 'jwt-decode';
import {AdminService} from '../../../services/admin/admin.service';
import {AuthService} from '../../../services/auth/auth.service';
import {UserRhService} from '../../../services/user-Rh/user-rh.service' ;
import {ConfigurationService} from '../../../services/configuration/configuration.service' ;
const uploadUri = environment.baseUrl + '/file/upload';
const awsUrlUri = environment.awsUrl;
import {DemandeService} from '../../../services/demande/demande.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent implements OnInit {
  uri = environment.baseUrl;
  form: FormGroup ;
  loginIn = false;
  permissionslist: any;
  uploader: FileUploader = new FileUploader({url: uploadUri});
  public image: string ;
  public permissions: string;
  public user: any ;
  public filePreviewPath: SafeUrl;
  param: string
  id: string;
  congeUtilise = 0;

  constructor(
    private adminService: AdminService,
    public authservice: AuthService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private demandeService : DemandeService,
    private sanitizer: DomSanitizer,
    private employeService: EmployeeService,
    private userRhService : UserRhService,
    private cService : ConfigurationService,
    private router: Router,
    private snackBar: MatSnackBar ) {

    this.uploader.onBeforeUploadItem = (item) => {
     item.withCredentials = false;
   }
   this.uploader.onAfterAddingFile = (fileItem) => {
     this.filePreviewPath  = this.sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(fileItem._file)));
     this.imageChange();
   }


   this.form = this.fb.group({
     password: ['', Validators.required],
     cpassword: new FormControl('', [
       passValidation,
     ]),
   });
   this.form.controls.password.valueChanges
   .subscribe(
     x => this.form.controls.cpassword.updateValueAndValidity()
   )
  }

   ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      if(params['id'])
      this.id = params['id'];
      else
      this.id=this.authservice.getUserfromToken()._id;
    if (this.id) {
      this.getSoldeCogeConsomme();
      this.adminService.getGroupes()
  .subscribe((res) => {
    this.permissionslist = res ;
    this.userRhService.getOneUser(this.id)
      .subscribe(res => {
        this.user = res;
        this.cService.getAllDeparts()
          .subscribe(departs => {
            this.user.departs = departs.filter(depart => this.user.departement.includes(depart._id));
        this.user.permsName = [];
        this.permissionslist.forEach(element => {
          if(this.user.permissions.includes(element._id)){
            this.user.permsName.push(' '+element.groupename);
          }
          
        });
       
        if (this.user.image !== '') {
        this.image = awsUrlUri + this.user.image ;
        } else {
          if (this.user.gender === 'female') {
            this.image = 'woman.png';
          } else {
            this.image = 'user.png';
          }
        }
      }, err => {
        this.router.navigate(['/user-profile'], { queryParams: { id: this.employeService.getIdfromToken()},
        skipLocationChange: true} );
      });
      }, err => {
        this.router.navigate(['/user-profile'], { queryParams: { id: this.employeService.getIdfromToken()},
        skipLocationChange: true} );
      });
    }, err => {
      console.log(err);
    });
    } else {
      this.router.navigate(['/user-profile'], { queryParams: { id: this.employeService.getIdfromToken() },
      skipLocationChange: true} );

    }
  });
}

fetchGroupes() {
  this.adminService.getGroupes()
  .subscribe((res) => {
    this.permissionslist = res ;
  }, err => {
    console.log(err);
  });
}

imageChange(){
  if (this.uploader.queue.length > 0) {
    this.uploader.queue[this.uploader.queue.length - 1].upload()
    this.uploader.onCompleteItem = (item: any, response: any , status: any, headers: any) => {
      let user = {
        image : JSON.parse(response).uploadname
      }
      this.employeService.updateUser(this.id,user).subscribe(data => {
        if (data.success) {
        this.snackBar.open(data.msg , '✔', {
          verticalPosition: 'top',
          panelClass: ['btn-success'],
            duration: 6000
          });
        } else {
          console.log(data);
          this.snackBar.open(data.msg , 'X', {
            verticalPosition: 'top',
            panelClass: ['btn-danger'],
            duration: 6000
          });
        }
       });
    }
    }
}

onSubmit() {
  if (!this.form.invalid && this.form.controls.password.value !== '') {
    this.loginIn = true;
    let user = {
      password : this.form.controls.password.value
    };
    this.employeService.updateUser(this.id, user).subscribe(data => {
      console.log(data);
      if (data.success) {
        this.loginIn = false;
        this.snackBar.open(data.msg , '✔', {
          verticalPosition: 'top',
          panelClass: ['btn-success'],
          duration: 6000
        });
      } else {
      console.log(data);
      this.snackBar.open(data.msg , 'X', {
        verticalPosition: 'top',
        panelClass: ['btn-danger'],
        duration: 6000
      });
      this.loginIn = false;
      }
     });
  }
}


solde(date,conge): number {
  let date1 = new Date();
  let date2 = new Date(date);
  var diff = Math.floor(date1.getTime() - date2.getTime());
  var days = Math.floor(diff/(1000 * 3600 * 24));
  return Math.floor(((days * (conge/12/30)))-this.congeUtilise);
  }

  getSoldeCogeConsomme(){
    this.demandeService.getSoldeCogeConsomme(this.id)
    .subscribe(congeUtilise => {
      if(congeUtilise.length!=0)
        this.congeUtilise = congeUtilise[0].periode;
      else
        this.congeUtilise = 0;
  }, err => {
    console.log(err);
  });
}
  
calcDate(date): string {
  let date1 = new Date();
  let date2 = new Date(date);
  var diff = Math.floor(date1.getTime() - date2.getTime());
  var day = 1000 * 60 * 60 * 24;

  var days = Math.floor(diff/day); //5151

  var years = Math.floor(days/365.25); //14

  days = days - 365.25 * years;

  var months = Math.floor(days/30);

  days = Math.floor(days - months * 30);
 
  let message = '';
  if(days < 1){
    message += days + " Jour, " ;
  } else {
    message += days + " Jours, " ;
  }
  message += months + " Mois, ";
  
  if(years < 1){
    message += years + " An " ;
  } else {
    message += years + " Ans " ;
  }
  return message
  }

}
