import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRhComponent } from './user-rh.component';

describe('UserRhComponent', () => {
  let component: UserRhComponent;
  let fixture: ComponentFixture<UserRhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserRhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
