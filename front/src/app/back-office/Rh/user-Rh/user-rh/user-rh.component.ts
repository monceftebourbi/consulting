import { Component, OnInit, HostListener } from '@angular/core';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import {UserRhService} from '../../../../services/user-Rh/user-rh.service' ;
import {AdminService} from '../../../../services/admin/admin.service' ;
import {EmployeeService} from '../../../../services/employee/employee.service';
import {AuthService} from '../../../../services/auth/auth.service';
import {emailValidation, cinValidation, ribValidation, passValidation} from '../../../../services/validations' ;
import {Router, ActivatedRoute} from '@angular/router' ;
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatOption } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { MatSnackBar } from '@angular/material/snack-bar';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import {  FileUploader } from 'ng2-file-upload';
import { environment } from '../../../../../environments/environment';
import { ConfigurationService } from '../../../../services/configuration/configuration.service';

export interface Agroupe  {
  id: string;
  groupename: string;
}

@Component({
  selector: 'app-user-rh',
  templateUrl: './user-rh.component.html',
  styleUrls: ['./user-rh.component.scss'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'fr'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ]
})
export class UserRhComponent implements OnInit {
  uri = environment.baseUrl;
  awsuri = environment.awsUrl;
  public filePreviewPath: SafeUrl;
  uploader: FileUploader = new FileUploader({url: this.uri+'/file/upload/'});
  public image: string ;
  permissionslist: Agroupe[];
  filteredPermissionslist: Agroupe[];
  personalForm: FormGroup ;
  accountForm: FormGroup ;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  user: any ;
  tab = 0;
  isMobile = false;
  isCDI = false;
  departs: any;
  users: any;
  myDepart: any;
  postes: any;
  depart = [];
  isLinear = true;
  value: string;
  viewValue: string;
  firstdate = new Date();
  id: string;
  responsables : any;
  departement : any;
  arrayOne(n: number): any[] {
    return Array(n);
  }
  constructor(
    private fb: FormBuilder,
    private sanitizer: DomSanitizer,
    private userRhService : UserRhService,
    private adminService : AdminService,
    private router: Router,
    private snackBar: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    private cService: ConfigurationService,
    private employeeService: EmployeeService,
    private authService: AuthService
  ) { 

    this.fetchGroupes();
    this.fetchPostes();
    this.fetchUsers();
    this.fetchDeparts();
    this.image = 'user.png' ;
    this.uploader.onBeforeUploadItem = (item) => {
      item.withCredentials = false;
    }
    this.uploader.onAfterAddingFile = (fileItem) => {
      this.filePreviewPath  = this.sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(fileItem._file)));
      console.log(this.filePreviewPath);
    }
    this.personalForm = this.fb.group({
      fname: ['', Validators.required],
      lname: ['', Validators.required],
      cin: new FormControl('', [
        Validators.required,
        cinValidation
      ]),
      adress: ['', Validators.required],
      sexe : ['', Validators.required],
      datenaissance: ['', Validators.required], 
      tel: new FormControl('', [
        Validators.required,
        Validators.min(10000000),
        Validators.max(99999999)
      ]), 
      image:'',
    });

    this.accountForm = this.fb.group({
      username: ['', Validators.required],
      email: new FormControl('', [
        Validators.required,
        emailValidation
      ]),
      password: ['', Validators.required],
      cpassword: new FormControl('', [
        Validators.required,
        passValidation,
      ]),
      permissions: ['', Validators.required],
    });


    this.firstFormGroup = this.fb.group({
      poste: [{value: '', disabled: true}, Validators.required],
      grade: [{value: '', disabled: true}, Validators.required],
      departement: ['', Validators.required],
      service: [{value: '', disabled: true}, Validators.required],
      responsable: [{value: '', disabled: true}, Validators.required],
      anciente: ['', Validators.required],
      datecommencement: ['', Validators.required],
    });

    this.secondFormGroup = this.fb.group({
      salairebrut: ['', Validators.required],
      salairenet: ['', Validators.required],
      cnss: [''],
      type: ['', Validators.required],
      datedebutc: ['', Validators.required],
      datefinc: ['', Validators.required],
      diplome: ['', Validators.required],
      specialite: ['', Validators.required],
      rib:  new FormControl('', [
        Validators.required,
        ribValidation
      ]),
      banque: ['', Validators.required],
      avantageNature: [''],
      avantageNatureValeur: [''],
      conge: ['', Validators.required],
      situationfamiliale: ['', Validators.required],
      nationalite: ['', Validators.required],
    });
  }
  
  ngOnInit() {
    if ((window.innerWidth) > 1283) {
      this.isMobile = false;
    }else{
      this.isMobile = true;
    }
    this.activatedRoute.queryParams.subscribe(params => {
      if(params['id'])
      this.id = params['id'];
      else
      this.id=this.authService.getUserfromToken()._id;
    if (this.id) {
    this.isLinear = false;
    this.accountForm.controls['password'].clearValidators();
    this.accountForm.controls['password'].updateValueAndValidity();
    this.accountForm.controls['cpassword'].clearValidators();
    this.accountForm.controls['cpassword'].updateValueAndValidity();
      this.userRhService.getOneUser(this.id)
      .subscribe(res => {
        this.user = res ;
        this.image = res.image;
        this.personalForm.patchValue({
            adress : this.user.adress,
            email : this.user.email,
            fname : this.user.fname,
            lname : this.user.lname,
            tel : this.user.tel,
            cin : this.user.cin,
            sexe :this.user.sexe,
            datenaissance : this.user.datenaissance,
           });
           this.accountForm.patchValue({
            username    : this.user.username,
            email       : this.user.email,  
            permissions : this.user.permissions,
          });
          this.firstFormGroup.patchValue({
            poste : this.user.poste,
            grade : this.user.grade.toString(),
            departement : this.user.departement,
            service : this.user.service,
            responsable : this.user.responsable,
            anciente : this.user.anciente,
            datecommencement : this.user.datecommencement,
          });
          this.secondFormGroup.patchValue({
            salairebrut : Number(this.user.contrat.salairebrut.toString().replace(/\s/g, '')).toFixed(3).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 '),
            salairenet : Number(this.user.contrat.salairenet.toString().replace(/\s/g, '')).toFixed(3).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 '),
            type : this.user.contrat.type,
            cnss : this.user.contrat.cnss,
            datedebutc : this.user.contrat.datedebutc,
            datefinc : this.user.contrat.datefinc,
            diplome : this.user.contrat.diplome,
            specialite : this.user.contrat.specialite,
            rib : this.user.contrat.rib,
            banque : this.user.contrat.banque,
            avantageNature : this.user.contrat.avantageNature,
            avantageNatureValeur : this.user.contrat.avantageNatureValeur,
            conge : this.user.contrat.conge,
            situationfamiliale : this.user.contrat.situationfamiliale,
            nationalite : this.user.contrat.nationalite,
           });
       
      }, err => {
        this.snackBar.open('Utisilsateur n\'existe pas', 'X', {
          verticalPosition: 'top',
          panelClass: ['btn-danger'],
          duration: 6000
        });
      });
    } 
  });
  }

  onSubmit() {
    if (this.secondFormGroup.valid && this.personalForm.valid && this.firstFormGroup.valid) { 
      this.user = {...this.accountForm.value,...this.personalForm.value,...this.firstFormGroup.value};
        this.user.contrat = this.secondFormGroup.value;
        if (this.uploader.queue.length > 0) {

          this.uploader.queue[this.uploader.queue.length - 1].upload()
        
          this.uploader.onCompleteItem = (item: any, response: any , status: any, headers: any) => {
            this.user.image = JSON.parse(response).uploadname;
            this.updateUser();
          }
        } else {
          this.user.image = this.image;
          this.updateUser();
        }
        console.log(this.user);
      }
    }

    updateUser(){
    if(this.id){
      this.userRhService.updateUser(this.id, this.user).subscribe(data => {
        if (data.success) {
          // this.router.navigate(['/users']);
          this.snackBar.open('Mise à jours avec succès', '✔', {
            verticalPosition: 'top',
            panelClass: ['btn-success'],
              duration: 6000
            });
          this.router.navigate(['/usersRh']);
        } else {
        this.snackBar.open('Une erreur s\'est produit', 'X', {
          verticalPosition: 'top',
          panelClass: ['btn-danger'],
          duration: 6000
        });
  
        }
       });

    } else {
        this.userRhService.registerUser(this.user).subscribe(data => {
          if (data.success) {
            this.router.navigate(['/usersRh']);
    
            this.snackBar.open(data.msg, '✔', {
              verticalPosition: 'top',
              panelClass: ['btn-success'],
                duration: 6000
              });
          } else {
              this.snackBar.open(data.msg, 'X', {
                verticalPosition: 'top',
                panelClass: ['btn-danger'],
                duration: 6000
              });
          }
         });
       }
    }

  selectTab(index: number) : void{
    this.tab= index;

  }
  numericOnly(event): boolean {
    const patt =  /^\d*\.?\d*$/;
    return patt.test(event.key);
  }
  estwi(event,control) {
    console.log(control);
    const tmp = Number(event.target.value.replace(/\s/g, '')).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
    this.firstFormGroup.controls[control].setValue(tmp);
   
  }
  calculanc(event) {

    let dateCom = event.target.value;    //let declarer une variable 
    let date1 = new Date();
    let date2 = new Date(dateCom);
    var diff = Math.floor(date1.getTime() - date2.getTime());
  var day = 1000 * 60 * 60 * 24;


  var days = Math.floor(diff/day); //5151

  var years = Math.floor(days/365.25); //14

  days = days - 365.25 * years;

  var months = Math.floor(days/30);

  days = Math.floor(days - months * 30);
 

  let message = '';
  if(days < 1){
    message += days + " Jour, " ;
  } else {
    message += days + " Jours, " ;
  }
  message += months + " Mois, ";
  
  if(years < 1){
    message += years + " An " ;
  } else {
    message += years + " Ans " ;
  }
      this.firstFormGroup.controls['anciente'].setValue(message);
  }

  onChangeDpart(newValue) {
    if(newValue){
      this.firstFormGroup.controls['service'].enable();
      this.firstFormGroup.controls['poste'].enable();
      this.firstFormGroup.controls['poste'].updateValueAndValidity();
      this.firstFormGroup.controls['grade'].enable();
      this.firstFormGroup.controls['grade'].updateValueAndValidity();
      this.firstFormGroup.controls['responsable'].enable();
      }else{
        this.firstFormGroup.controls['poste'].setValue('');
        this.firstFormGroup.controls['grade'].setValue('');
        this.firstFormGroup.controls['service'].disable();
        this.firstFormGroup.controls['poste'].disable();
        this.firstFormGroup.controls['grade'].disable();
        this.firstFormGroup.controls['responsable'].disable();
      }
      if(this.user && this.user.departement && newValue == this.user.departement)
      this.firstFormGroup.patchValue({
        service : this.user.service,
        responsable : this.user.responsable
      });
      this.myDepart = this.departs.filter(depart => newValue.some(newVal => newVal === depart._id))
      this.myDepart.services = [];
      this.myDepart.forEach(depart => {
        this.myDepart.services.push(...depart.services);
      });
    this.depart = newValue;
    

 }

 onChangeType(newValue) {
    if(newValue && newValue == 'CDI'){
      this.secondFormGroup.controls['datefinc'].clearValidators();
      this.secondFormGroup.controls['datefinc'].updateValueAndValidity();
      this.isCDI = true;
    } else {
      this.secondFormGroup.controls['datefinc'].setValidators([Validators.required]);
      this.isCDI = false;
    }
 }

 onChangeGrade(newValue) {
     this.responsables = this.users.filter(user => user.grade > newValue && user._id !== this.id);
  if(newValue == 100){
    this.firstFormGroup.controls['responsable'].clearValidators();
    this.firstFormGroup.controls['responsable'].updateValueAndValidity();
  }else{
    this.firstFormGroup.controls['responsable'].setValidators([Validators.required]);
  }
}

 fetchGroupes() {
  this.adminService.getGroupes()
  .subscribe((res: Agroupe[]) => {
    this.permissionslist = res ;
      this.filteredPermissionslist = this.permissionslist.slice();
  }, err => {
    console.log(err);
  });
}

 fetchPostes() {
  this.cService.getPostes()
  .subscribe((res) => {
    this.postes = res.postes;
    console.log(this.postes);
    console.log(res);
   }, err => {
    console.log(err);
  });
}



fetchUsers() {
  this.employeeService.getUsers()
  .subscribe((res) => {
    this.users = res ;
    console.log(this.users);
  }, err => {
    console.log(err);
  });
}

@HostListener('window:resize', ['$event'])
onResize(event) {
  if (event.target.innerWidth > 1283) {
    this.isMobile = false;
  }else{
    this.isMobile = true;
  }
}


fetchDeparts(){
  this.cService.getAllDeparts()
  .subscribe(res => {
  this.departs =  res;
  console.log(this.departs);
  }, err => {
    console.log(err);
  });
}
  
}



