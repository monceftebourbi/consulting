import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersRhComponent } from './users-rh.component';

describe('UsersRhComponent', () => {
  let component: UsersRhComponent;
  let fixture: ComponentFixture<UsersRhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersRhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersRhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
