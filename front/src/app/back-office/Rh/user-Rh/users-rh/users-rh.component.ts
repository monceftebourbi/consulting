import { Component, OnInit, ViewChild } from '@angular/core';
import {UserRhService} from '../../../../services/user-Rh/user-rh.service' ;
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { environment } from '../../../../../environments/environment' ;
import {ConfigurationService} from '../../../../services/configuration/configuration.service';
import * as decode from 'jwt-decode';



@Component({
  selector: 'app-users-rh',
  templateUrl: './users-rh.component.html',
  styleUrls: ['./users-rh.component.scss']
})
export class UsersRhComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  uri = environment.baseUrl;
  mypermissions: any[];
  MyDataSource: any;
  displayedColumns: string[] = ['username','nom','tel' ,'email', 'poste','departement', 'action'];

public Users : any[] ;
public departs : any ;
public user : any ;
public groupes : any[] ;

public deleteId :string ;

  constructor(private userRhService: UserRhService,
              private cService: ConfigurationService, 
              private snackBar: MatSnackBar,) { }

  ngOnInit() {
    this.fetchData();
  
  }
  
  fetchData(){
    this.cService.getAllDeparts()
        .subscribe(res => {
        this.departs =  res;
        this.userRhService.getUsers()
      .subscribe(res2 => {
        this.MyDataSource = new MatTableDataSource();
        
        res2.forEach((user, index) => {
          if(user.active){
            user.depart = [];
          this.departs.forEach(depart => {
            if(user.departement.includes(depart._id))
            user.depart.push(depart.name);
          });
         }else{
           res2.splice(index,1)
         }
        });
        console.log(res2);
        this.MyDataSource.data = res2.filter(user => user.active);;
        this.MyDataSource.paginator = this.paginator;
        this.MyDataSource.sort = this.sort;
        this.Users = res2 ;
        console.log(this.Users);
      }, err => {
        console.log(err);
      });
  }, err => {
    console.log(err);
  });

  }
  togglModal(id){
    this.deleteId = id;
  }
  delete(id){
 
    this.user = this.Users.find(user => user._id == id);
    console.log(this.user);
    let desactivate = {
      active : false
    }
      this.userRhService.updateUser(id,desactivate)
        .subscribe(res => {
            this.fetchData();
            let element :HTMLElement = document.getElementById('hideModal') as HTMLElement;
            element.click();
            this.snackBar.open('Utilisateur supprimé avec succès', 'X', {
              duration: 6000
            });
          }, (err) => {
            console.log(err);
          }
        );

    }
    applyFilter(filterValue: string) {
      this.MyDataSource.filter = filterValue.trim().toLowerCase();
    }
  
}
