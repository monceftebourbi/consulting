import { Component, OnInit, ViewChild } from '@angular/core';
import {UserRhService} from '../../../../services/user-Rh/user-rh.service' ;
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { environment } from '../../../../../environments/environment' ;
import {ConfigurationService} from '../../../../services/configuration/configuration.service';
import {DemandeService} from '../../../../services/demande/demande.service';
import {NotificationService} from '../../../../services/notification/notification.service';
import {AuthService} from '../../../../services/auth/auth.service';

export interface linkModel{
  url? : string ;
  params? : string ;
  paramsValue?:string ;
}

@Component({
  selector: 'app-conges-rh',
  templateUrl: './conges-rh.component.html',
  styleUrls: ['./conges-rh.component.scss']
})
export class congesRhComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  uri = environment.baseUrl;
  mypermissions: any[];
  public notification: any ;
  public notifLink: linkModel = {} ;
  MyDataSource: any;
  displayedColumns: string[] = ['Type','User','Solde','Datesoumission','Datedebut','Datefin','Etat'];

conges : any[] ;
confirmationId :string ;

  constructor(private userRhService: UserRhService,
              private demandeService : DemandeService,
              private authService : AuthService,
              private notificationService : NotificationService,    
              private snackBar: MatSnackBar,) { }

  ngOnInit() {
    this.fetchData();
  
  }
  
  fetchData(){
    this.demandeService.getConges()
        .subscribe(res => {
        this.userRhService.getUsers()
      .subscribe(res2 => {
        this.MyDataSource = new MatTableDataSource();
        res.forEach(conge => {
          conge.user = res2.find(user => user._id == conge.user);
          console.log();
        });
        console.log(res2);
        this.MyDataSource.data = res;
        this.MyDataSource.paginator = this.paginator;
        this.MyDataSource.sort = this.sort;
        this.conges =  res;
      }, err => {
        console.log(err);
      });
  }, err => {
    console.log(err);
  });

  }
  togglModal(id){
    this.confirmationId = id;
  }

  confirmation(confirmationId,etat){
    let conge = {
      etat : etat 
    };
    this.demandeService.updateconge(confirmationId,conge)
        .subscribe(res => {
            let element :HTMLElement = document.getElementById('hideModal') as HTMLElement;
            element.click();
            this.snackBar.open('Congé Modifier', '✔', {
             verticalPosition: 'top',
             panelClass: ['btn-success'],
              duration: 6000
            });
            this.notifLink.url = '/user-profile';
            let conge = this.conges.filter(conge => conge._id === confirmationId)[0];
            this.notification = {
              'sender': this.authService.getUserfromToken()._id,
              'receiver': conge.user,
              'title': 'Demande de congé',
              'image': this.authService.getUserfromToken().image,
              'message': 'Votre demande de congé à été traité',
              'link': this.notifLink,
            };
            this.notificationService.createNotification(JSON.stringify(this.notification)).subscribe(data2 => {
              if (data2.success) {
                console.log(this.notification);
                console.log(data2);
              }
            });
            this.fetchData();
          }, (err) => {
            console.log(err);            
          this.snackBar.open('une erreur c\'est produit' , 'X', {
            verticalPosition: 'top',
            panelClass: ['btn-danger'],
            duration: 6000
          });
          this.fetchData();  
        }
        );
    }

    applyFilter(filterValue: string) {
      this.MyDataSource.filter = filterValue.trim().toLowerCase();
    }
  
}
