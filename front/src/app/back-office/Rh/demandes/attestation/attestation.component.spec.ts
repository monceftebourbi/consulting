import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttestationRhComponent } from './attestation.component';

describe('AttestationRhComponent', () => {
  let component: AttestationRhComponent;
  let fixture: ComponentFixture<AttestationRhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttestationRhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttestationRhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
