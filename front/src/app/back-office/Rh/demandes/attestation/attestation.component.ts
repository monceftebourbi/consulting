import { Component, OnInit, ViewChild, ChangeDetectorRef, ElementRef } from '@angular/core';
import {UserRhService} from '../../../../services/user-Rh/user-rh.service' ;
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { environment } from '../../../../../environments/environment' ;
import {ConfigurationService} from '../../../../services/configuration/configuration.service';
import {DemandeService} from '../../../../services/demande/demande.service';
import {NotificationService} from '../../../../services/notification/notification.service';
import {AuthService} from '../../../../services/auth/auth.service';
import { FileUploader } from 'ng2-file-upload';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';


const uri = environment.baseUrl+'/file/upload/';


export interface linkModel{
  url? : string ;
  params? : string ;
  paramsValue?:string ;
}

@Component({
  selector: 'app-attestation-rh',
  templateUrl: './attestation.component.html',
  styleUrls: ['./attestation.component.scss']
})
export class AttestationRhComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('closeModal', {static: true} ) closeModal: ElementRef;
  uri = environment.baseUrl;
  mypermissions: any[];
  public notification: any ;
  public notifLink: linkModel = {} ;
  uploader: FileUploader = new FileUploader({url: uri, removeAfterUpload: true , allowedFileType: ['image', 'doc', 'pdf', 'xls', 'ppt']});
  filePreviewPath: SafeUrl;
  hasBaseDropZoneOver:boolean = false;
  hasAnotherDropZoneOver:boolean = false;
  submited = false;
  removing = false;
  MyDataSource: any;
  displayedColumns: string[] = ['Type','User','Raison','Datesoumission','Etat'];
  doc: any;
Attestation : any[] ;
confirmationId :String ;

  constructor(private userRhService: UserRhService,
              private demandeService : DemandeService,
              private authService : AuthService,
              private changeDetectorRef: ChangeDetectorRef,
              private sanitizer: DomSanitizer,          
              private notificationService : NotificationService,    
              private snackBar: MatSnackBar,) { 
                this.uploader.onBeforeUploadItem = (item) => {
                  item.withCredentials = false;             
                 }
                  this.uploader.onAfterAddingFile = (fileItem) => {
                    this.filePreviewPath  = this.sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(fileItem._file)));
                   }
              }

  ngOnInit() {
    this.fetchData();
  
  }
  
  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }
 
  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }
  rotateArrow(section,action){
    let files : HTMLElement = document.getElementById(section) as HTMLElement;
    if(!files.classList.contains('down')){
      files.classList.add('down') ;
    }else{
      files.classList.remove('down');
    }
  }

  removeFile(item){
    this.removing = true;
    item.remove();
    setTimeout(()=>{    
      this.removing = false;
    }, 500); 
    this.changeDetectorRef.detectChanges();
  }

  fetchData(){
    this.demandeService.getAllAttestations()
        .subscribe(res => {
        this.userRhService.getUsers()
      .subscribe(res2 => {
        this.MyDataSource = new MatTableDataSource();
        res.forEach(conge => {
          conge.user = res2.find(user => user._id == conge.user);
          console.log();
        });
        console.log(res2);
        this.MyDataSource.data = res;
        this.MyDataSource.paginator = this.paginator;
        this.MyDataSource.sort = this.sort;
        this.Attestation =  res;
      }, err => {
        console.log(err);
      });
  }, err => {
    console.log(err);
  });

  }
  togglModal(id){
    this.confirmationId = id;
  }

  confirmation(confirmationId,etat){
    this.submited=true;
    let attestation = {
      etat : etat,
      file : '' 
    };
     if (this.uploader.queue.length > 0) {
              for(var i =0;i<this.uploader.queue.length;i++){
              this.uploader.queue[i].upload()
              this.uploader.onCompleteItem = (item: any, response: any , status: any, headers: any) => {
                if(response)
                 this.doc = JSON.parse(response).uploadname;
                else{
                  this.snackBar.open('Une erreur c\'est produit verifier votre connection', 'X', {
                    verticalPosition: 'top',
                    panelClass: ['btn-danger'],
                    duration: 6000
                  });
                }
              }
            }
          }

      if(this.uploader.queue.length > 0){
      this.uploader.onCompleteAll = () => {
        attestation.file = this.doc;
        this.demandeService.updateAttestation(confirmationId,attestation).subscribe(data => {
          if (data.success) {
            this.snackBar.open(data.msg, '✔', {
             verticalPosition: 'top',
             panelClass: ['btn-success'],
              duration: 6000
            });
            this.notifLink.url = '/user-profile';
            let attestation = this.Attestation.filter(conge => conge._id === confirmationId)[0];
            this.notification = {
              'sender': this.authService.getUserfromToken()._id,
              'receiver': attestation.user,
              'title': 'Attestation',
              'image': this.authService.getUserfromToken().image,
              'message': 'Votre '+ attestation.type+' est prête',
              'link': this.notifLink,
            };
            this.notificationService.createNotification(JSON.stringify(this.notification)).subscribe(data2 => {
              if (data2.success) {
                console.log(this.notification);
                console.log(data2);
              }
            });
            this.fetchData();
            this.closeModal.nativeElement.click() //<-- here
          } else {
            this.snackBar.open(data.msg, 'X', {
              verticalPosition: 'top',
              panelClass: ['btn-danger'],
               duration: 6000
             });
          }
          this.submited = false;
         });
      }
    }else{
      this.demandeService.updateAttestation(confirmationId,attestation).subscribe(data => {
        if (data.success) {
          this.snackBar.open(data.msg, '✔', {
           verticalPosition: 'top',
           panelClass: ['btn-success'],
            duration: 6000
          });
          this.notifLink.url = '/user-profile';
            let attestation = this.Attestation.filter(conge => conge._id === confirmationId)[0];
            this.notification = {
              'sender': this.authService.getUserfromToken()._id,
              'receiver': attestation.user,
              'title': 'Attestation',
              'image': this.authService.getUserfromToken().image,
              'message': 'Votre '+ attestation.type+' est prête',
              'link': this.notifLink,
            };
            this.notificationService.createNotification(JSON.stringify(this.notification)).subscribe(data2 => {
              if (data2.success) {
                console.log(this.notification);
                console.log(data2);
              }
            });
            this.fetchData();
            this.closeModal.nativeElement.click() //<-- here
        } else {
          this.snackBar.open(data.msg, 'X', {
            verticalPosition: 'top',
            panelClass: ['btn-danger'],
             duration: 6000
           });
        }
        this.submited = false;
       });
    }
    }

    applyFilter(filterValue: string) {
      this.MyDataSource.filter = filterValue.trim().toLowerCase();
    }
  
}
