import { Component, OnInit , Inject} from '@angular/core';
import {FormGroup, FormBuilder, FormControl, Validators} from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';
import {AdminService} from '../../../services/admin/admin.service';
import {ConfigurationService} from '../../../services/configuration/configuration.service';
import { async } from '@angular/core/testing';

const uri = environment.baseUrl;
@Component({
  selector: 'app-groupe-action',
  templateUrl: './groupe-action.component.html',
  styleUrls: ['./groupe-action.component.scss']
})
export class GroupeActionComponent implements OnInit {
  permissionslist: string[][] = [
                                ['Crée', 'Create'],
                                ['Mise à jours', 'Update'],
                                ['Voir', 'View'],
                                ['Supprimer', 'Delete']];
  form: FormGroup ;
  dynamicForm: FormGroup ;
  public groupe: any ;
  param: string
  id: string;
  departs: any;
  constructor(
    public dialogRef: MatDialogRef<GroupeActionComponent>,
    private adminService: AdminService,
    private configurationService: ConfigurationService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data: any

    ) {    
      this.form = this.fb.group({
        groupename: ['', Validators.required],
        appels: [''],
        encaissement: [''],
        decaissement: [''],
        reglement: [''],
        bill: [''],
        users: [''],
        dashboard: [''],
        clients: [''],
        groupes: [''],
        rh: [''],
        config: [''],
      });
    }

    ngOnInit() {
     this.id = this.data.id;
     if (this.id) { 
          this.configurationService.getAllDeparts()
          .subscribe( async res => {    
          this.departs = res;    
            let group={}
            res.forEach(depart=>{
              group[depart._id] = new FormControl('');  
              this.form.addControl(depart._id,new FormControl(''))
            })
            console.log(res);
          this.adminService.getOneGroupe(this.id)
          .subscribe(res => {
            this.groupe = res ;
            Object.keys(this.form.controls).forEach(key => {
              this.form.controls[key].setValue(this.filterItems(res.permissions, key));
            });
            this.form.controls['groupename'].setValue(res.groupename);
          }, err => {
              console.log(err);
          });
        }, err => {
          console.log(err);
        });
     }else{
      this.configurationService.getAllDeparts()
      .subscribe( async res => {    
      this.departs = res;    
        let group={}
        res.forEach(depart=>{
          group[depart._id] = new FormControl('');  
          this.form.addControl(depart._id,new FormControl(''))
        })
        console.log(res);
      }, err => {
        console.log(err);
      });
       
     }
 }

 onSubmit() {
  if (this.id){
   if (!this.form.invalid) {
   this.adminService.updateGroupe(this.id, this.form.value).subscribe(data => {
    if (data.success) {
      this.snackBar.open(data.msg, 'X', {
        duration: 6000
      });
      this.dialogRef.close();
    } else {
        this.snackBar.open(data.msg, 'X', {
          duration: 6000
        });
    }
   });
  }
 } else {
   // create new groupe
   if (!this.form.invalid) {
   this.adminService.createGroupe(this.form.value).subscribe(data => {
    if (data.success) {
      this.snackBar.open(data.msg, 'X', {
        duration: 6000
      });
      this.dialogRef.close();
    } else {
        this.snackBar.open(data.msg, 'X', {
          duration: 6000
        });
    }
   });
  }
 }

 }
 filterItems = (arr, query) => {
  return arr.filter(el => el.toLowerCase().indexOf(query.toLowerCase()) > -1);
};

}
