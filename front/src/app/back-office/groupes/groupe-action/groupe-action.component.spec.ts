import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupeActionComponent } from './groupe-action.component';

describe('GroupeActionComponent', () => {
  let component: GroupeActionComponent;
  let fixture: ComponentFixture<GroupeActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupeActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupeActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
