import { Component, OnInit, ViewEncapsulation, ViewChild ,AfterViewInit} from '@angular/core';
import {AdminService} from '../../services/admin/admin.service';
import {ManagementService} from '../../services/management/management.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { GroupeActionComponent } from './groupe-action/groupe-action.component';
import { animate, state, style, transition, trigger } from '@angular/animations';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import { DataSource } from '@angular/cdk/table';

export interface Aclient  {
  id: string;
  fname: string;
  lname: string;
  adress: string;
  email: string;
}

export interface GroupesAPI {
  items: any[];
  total_count: number;
}


@Component({
  selector: 'app-groupes',
  templateUrl: './groupes.component.html',
  styleUrls: ['./groupes.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', visibility: 'hidden'})),
      state('expanded', style({height: '*', visibility: 'visible'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})





export class GroupesComponent implements OnInit,AfterViewInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  aclientList: Aclient[];
  public groupes: any;

  // Important objects
  services: any ;
  expandedElement: Aclient | null;
  MyDataSource: any;
  displayedColumns: string[] = ['groupename', 'action'];
  resultsLength = 0;
  data = [] ;
  page = 0 ;
  tableSort = 1;
  isLoadingResults = true;
  currentId = 0;
  action = 1 ;
  public deleteId: string ;
  constructor(
    private mService: ManagementService,
    private adminService: AdminService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    ) { }

  ngOnInit() {
   this.getData(this.paginator,1,this.currentId);

  }


  ngAfterViewInit() {
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() =>{    
      this.currentId = 0 ;
      this.tableSort *= -1 ;
      this.data = [];
      this.paginator.pageIndex = 0 ;
    });
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          console.log(this.data);
          console.log(this.tableSort);
          console.log(this.currentId);

          if(this.data.length > 0 ){
          this.currentId = this.data[this.data.length - 1]._id;
          }
          if(this.paginator.pageIndex > this.page){
            this.action = 1;
            console.log("akber");
          }else{
            if(this.paginator.pageIndex < this.page){
            this.action = 0;
            console.log("asgher");  
          }
          }
          this.page = this.paginator.pageIndex ;
        
          console.log(this.paginator.pageIndex);
          console.log(this.action);
          console.log(this.currentId);
          return this.getData(this.paginator,this.action, this.currentId);
          
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.resultsLength = data.total_count;
          this.MyDataSource = new MatTableDataSource(data.items);
          this.data = data.items ;
          return data.items;
        })
      ).subscribe(data =>console.log(data));
  }

  getData(page: any,action : Number, currentId : Number){
    return this.adminService.getGroupesToTable(page.pageSize,this.tableSort,action,currentId);
  }




  togglModal(id) {
    this.deleteId = id;
  }

  delete(id) {
    this.adminService.deleteGroupe(id)
      .subscribe(res => {
        this.getData(this.paginator,this.action, this.currentId);
        const element: HTMLElement = document.getElementById('hideModal') as HTMLElement;
          element.click();
          this.snackBar.open('Groupe supprimé avec succès', 'X', {
            duration: 6000
          });
        }, (err) => {
          console.log(err);
        }
      );
  }


  openForm(groupeId = null) {
    const configDialog = new MatDialogConfig();
    configDialog.width = '65%';
    configDialog.panelClass = 'custom-dialog-container' ;
    configDialog.height = "100%"
    configDialog.data = {
      id: groupeId,
  };
  const dialogRef = this.dialog.open(GroupeActionComponent, configDialog);
    dialogRef.afterClosed().subscribe(result => {
      this.getData(this.paginator,this.action, this.currentId);
  });

  }

 

  applyFilter(filterValue: string) {
    this.MyDataSource.filter = filterValue.trim().toLowerCase();
  }

}
