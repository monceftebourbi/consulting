import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { CommonModule, registerLocaleData  } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { UserProfileComponent } from '../user/user-profile/user-profile.component';
import { NotificationsComponent } from '../notifications/notifications.component';
import { FileSelectDirective, FileDropDirective, FileUploader,FileUploadModule } from 'ng2-file-upload';
import localeFr from '@angular/common/locales/fr';
import { GroupesComponent } from '../groupes/groupes.component';
import { GroupeActionComponent } from '../groupes/groupe-action/groupe-action.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { UserRhComponent } from '../Rh/user-Rh/user-rh/user-rh.component';
import { UsersRhComponent } from '../Rh/user-Rh/users-rh/users-rh.component';
import { congesRhComponent } from '../Rh/demandes/conge/conges-rh.component';
import { AttestationRhComponent } from '../Rh/demandes/attestation/attestation.component';
import { CongeComponent } from '../user/user-profile/conge/conge.component';
import { AttestationComponent } from '../user/user-profile/attestation/attestation.component';
import {MatStepperModule} from '@angular/material/stepper';


import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule, MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectFilterModule } from 'mat-select-filter';
import { AuthService } from 'app/services/auth/auth.service';



registerLocaleData(localeFr);

@NgModule({
  imports:[
    MatStepperModule,
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    FileUploadModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatInputModule,
    MatMenuModule,
    MatSelectModule,
    MatDialogModule,
    MatTooltipModule,
    MatProgressBarModule,
    NgxMatSelectSearchModule,
    MatListModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatSelectFilterModule
  ],
  exports: [
    MatSelectFilterModule,
    MatIconModule
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    NotificationsComponent,
    GroupesComponent,
    GroupeActionComponent,
    UsersRhComponent,
    congesRhComponent,
    AttestationRhComponent,
    UserRhComponent,
    CongeComponent,
    AttestationComponent
  ]
})


export class AdminLayoutModule {}
