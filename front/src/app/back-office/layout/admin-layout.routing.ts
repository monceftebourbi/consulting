import { Routes } from '@angular/router';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { UserProfileComponent } from '../user/user-profile/user-profile.component';
import { NotificationsComponent } from '../notifications/notifications.component';
import { AuthGuard } from '../../services/auth/auth.guard';
import { RoleGuard } from '../../services/auth/role.guard';
import { GroupesComponent } from '../groupes/groupes.component';
import { GroupeActionComponent } from '../groupes/groupe-action/groupe-action.component';
import { UserRhComponent } from '../Rh/user-Rh/user-rh/user-rh.component';
import { congesRhComponent } from '../Rh/demandes/conge/conges-rh.component';
import { AttestationRhComponent } from '../Rh/demandes/attestation/attestation.component';
import { UsersRhComponent} from '../Rh/user-Rh/users-rh/users-rh.component';
import { CongeComponent } from './../user/user-profile/conge/conge.component';
import { AttestationComponent } from '../user/user-profile/attestation/attestation.component';
import { RegisterComponent } from 'app/register/register.component';





export const AdminLayoutRoutes: Routes = [

    { path: 'dashboard',      component: DashboardComponent, canActivate: [RoleGuard],
            data: { expectedRole: 'dashboard', title : 'Tableau de bord' } },
            
    { path: 'user-profile',   component: UserProfileComponent, canActivate:[AuthGuard],
                data: { title : 'Profile'} },    
    
    { path: 'groupe-action',          component: GroupeActionComponent, canActivate: [RoleGuard],
    data: { expectedRole: 'groupesUpdate', title : 'Groupe action' } },

    { path: 'groupes',  component: GroupesComponent, canActivate: [RoleGuard],
    data: { expectedRole: 'groupes', title : 'Groupes' } },

    { path: 'notifications',  component: NotificationsComponent, canActivate:[AuthGuard],
                data: { title : 'Notifications'} },
   
    { path: 'conge',          component: CongeComponent, canActivate:[AuthGuard] },

    { path: 'attestation',          component: AttestationComponent, canActivate:[AuthGuard] },

    { path: 'conges-Rh',          component: congesRhComponent, canActivate: [RoleGuard],
    data: { expectedRole: 'rh', title : 'Liste des demandes de congés'} },

    { path: 'AttestationRh',          component: AttestationRhComponent, canActivate: [RoleGuard],
    data: { expectedRole: 'rh', title : 'Liste des demandes d\'attestation'} },
   
    { path: 'userRh',   component: UserRhComponent, canActivate: [RoleGuard],
    data: { expectedRole: 'users', title : 'Collaborateur' }  },
    
    { path: 'usersRh',   component: UsersRhComponent, canActivate: [RoleGuard],
    data: { expectedRole: 'users', title : 'Gestion des collaborateurs' }  }, 
  
    
];
