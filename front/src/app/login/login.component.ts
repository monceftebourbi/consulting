import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth/auth.service';
import {Router} from '@angular/router' ;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email : string ;
  password : string ;
  error : string ;
  validation:string;
  loginIn = false;
  

  constructor(
     private authService:AuthService,
     private router:Router
     ) { 

  }

  ngOnInit() {
  }

  onLoginSubmit(){
    this.loginIn = true;
    const user = {
      email : this.email,
      password : this.password
    }
      if(!this.email || !this.password){
        this.validation = 'Les champs email et mot de passe sont obligatoires';
        this.loginIn = false;
      } else {
      this.authService.authenticateUser(user).subscribe(data =>{
       if(data.success){
          this.authService.sotreUserData(data.token, data.user);
          this.router.navigate(['/dashboard']);
       } else{
        
            this.error = data.msg ; 
       }
       this.loginIn = false;
      });
    }
  }

}
