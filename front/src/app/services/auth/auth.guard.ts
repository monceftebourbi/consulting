import {Injectable} from '@angular/core'; 
import {Router, CanActivate} from '@angular/router';
import {AuthService} from './auth.service';
import * as decode from 'jwt-decode';


@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authservice: AuthService,
        private router: Router) {
    }

    canActivate(){
        const token = localStorage.getItem('id_token');
        const tokenPayload: any = decode(token);
        
        if(this.authservice.loggedIn()){
           if (!tokenPayload.isClient) {
            return true ;
        } else{
            this.router.navigate(['/home']);
            return false ;
        }

    } else {
        this.router.navigate(['/login']);
        return false ;
    }
    }


}