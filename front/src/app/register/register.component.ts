import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import { passValidation } from 'app/services/validations';
import {UserService} from '../services/user/user.service'
import { Router } from '@angular/router';

import {MatSnackBar} from '@angular/material/snack-bar';
import states from '../../states.json';
import countries from '../../countries.json';
import cities from '../../cities.json';




@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  
//etats
  states = states.states;
//pays
  countries = countries.countries;
  // ville
  cities = cities.cities;

  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  userForm: FormGroup;


  
  formejuridiques: string[] = [
    'Entreprise individuelle', 'Société à responsabilité limitée (SARL)', 'Société unipersonnelle à responsabilité limitée (SUARL)',
    'Société anonyme (SA)','Société en commandite par actions (SCA)'
  ];
  secteurs: string[] = [
    'Les industries manufacturières', 'Services informatiques', 'Les communications',
    'Transport', 'Education et enseignement', 'Santé','Activités de production et d’industries culturelles',
    'Animation des jeunes les loisirs l\'encadrement de l\'enfance et la protection des personnes âgées',
    'Protection de l\'environnement','Formation professionnelle','Travaux publics','Promotion immobilière',
    'Services d\'études, de conseils, d\'expertises et d\'assistance','Services de recherche- développement',
    'Autres services',

  ];
  
  formSubmitAttempt: boolean=false;
  formSuccess: boolean
  user: any;
  email: any;
  password: any;
  

  

  constructor(private _formBuilder: FormBuilder,  
      private userService: UserService,
      private router :Router,
      private snackBar: MatSnackBar



    ) {
    this.firstFormGroup = this._formBuilder.group({
      raisonsocial: ['', Validators.required],
      formejuridique: ['', Validators.required],
      secteur: ['', Validators.required],
      autresecteur: [''],
      Date: ['', Validators.required],
      sigle: ['', Validators.required],
      enseigne: ['', Validators.required],
      matriculefiscal:new FormControl ('', [Validators.required])


    });
    this.secondFormGroup = this._formBuilder.group({

      adresse: ['', Validators.required],
      codepostal: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]),
      tel: new FormControl('', [Validators.required, Validators.min(10000000), Validators.max(90000000)]),
      fax: '',
      representant: ['', Validators.required],
      ville: ['', Validators.required],
      etat: ['', Validators.required],
      pays: ['', Validators.required]
    });

    this.thirdFormGroup = this._formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: ['', Validators.required],
      confirmermotdepasse: new FormControl('', [passValidation])
    });
    this.thirdFormGroup.controls.password.valueChanges.subscribe(
      x => this.thirdFormGroup.controls.confirmermotdepasse.updateValueAndValidity()
    )
  }
  
  onSubmit() {

    this.formSubmitAttempt = true;
    if (this.firstFormGroup.valid && this.secondFormGroup.valid && this.thirdFormGroup.valid) {
      this.user = {
        ...this.firstFormGroup.value,
        ...this.secondFormGroup.value,
        ...this.thirdFormGroup.value
      };
      console.log('form submitted');
      console.log(this.user);
      this.userService.createUser(this.user).subscribe(res=>{
        if(res.success)
        {
          this.formSuccess=true;
          this.snackBar.open(res.msg, '✔', {
            duration: 2000,
            verticalPosition: 'top',
            panelClass: ['btn-success']
          });
        }else{
          this.formSuccess =false;
          this.snackBar.open(res.msg, 'x', {
            duration: 2000,
            verticalPosition: 'top',
            panelClass: ['btn-danger']
          });
        }
      })  
    }

       /* //Register user
        this.authservice.createUser(user).subscribe(data =>{
          if (data.success){
            this.router.navigate(['/login']);
          }else {
            this.router.navigate(['/register']);
          }
        } ); */
  }

  fn(){
    if(this.firstFormGroup.controls.secteur.value == 'Autres services'){
      this.firstFormGroup.controls["autresecteur"].setValidators([Validators.required]);
    } else {
      this.firstFormGroup.controls["autresecteur"].clearValidators()
    }
    this.firstFormGroup.controls["autresecteur"].updateValueAndValidity();
  }

  ngOnInit() {
console.log(this.states);
console.log(this.countries);
console.log(this.cities);
  }

}



